# 1brc

Late to the party but still a fun way to to learn a new language and to see
how much you remember about the old ones.

The task is to compute the min, mean, and max temps grouped by wather station
and then print it sorted by station name as `{station=min/mean/max, ...}`.

Implemented (naive):
* (C) Python
* PHP
* Node.js

Up next:
* Go
* Java
* Elixir or Gleam?
* Rust?
* Haskelllll?
* Nim
* Zig
* Ruby
