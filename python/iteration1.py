# Let's replace regexes with custom parsing.

import sys

stats = {} # city => tuple of min, max, sum, count

def parse(line: str) -> tuple[str, float]:
    for i, c in enumerate(line[1:-3]): # name may not be empty, shortest end is d:dd\n
        if c == ";":
            return (line[0:i + 1], float(line[i + 2:-1]))

# as the name is probably longer than the temp, reading from the end might be faster
def parse2(line: str) -> tuple[str, float]:
    for i in range(-5, -len(line), -1):
        if line[i] == ";":
            return (line[0:i], float(line[i + 1:-1]))

with open(sys.argv[1], "r") as f:
    while (reading := f.readline()) != "":
        city, temp = parse2(reading)
        try:
            stat = stats[city]
        except KeyError:
            stat = (50.0, -20.0, .0, 0)
        stats[city] = (
            temp if temp < stat[0] else stat[0],
            temp if temp > stat[1] else stat[1],
            stat[2] + temp,
            stat[3] + 1
        )

sorted_cities = sorted(stats.keys())
print("{", sep="", end="")
print(", ". join([f"{city}={stats[city][0]}/{round(stats[city][2] / stats[city][3], 1)}/{stats[city][1]}" for city in sorted_cities]), end="")
print("}", sep="", end="")

# This performs even slightly worse than the regex one!
# Probably because it's C stuff whereas there's a lot of to and fro with looking
# at each char in python.

# parse2 is actually faster!
