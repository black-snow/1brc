# inlined v1

import sys

stats = {} # city => tuple of min, max, sum, count

with open(sys.argv[1], "r") as f:
    while (reading := f.readline()) != "":
        for i in range(-5, -len(reading), -1):
            if reading[i] == ";":
                city, temp = (reading[0:i], float(reading[i + 1:-1]))
                break
        try:
            stat = stats[city]
        except KeyError:
            stat = (50.0, -20.0, .0, 0)
        stats[city] = (
            temp if temp < stat[0] else stat[0],
            temp if temp > stat[1] else stat[1],
            stat[2] + temp,
            stat[3] + 1
        )

sorted_cities = sorted(stats.keys())
print("{", sep="", end="")
print(", ". join([f"{city}={stats[city][0]}/{round(stats[city][2] / stats[city][3], 1)}/{stats[city][1]}" for city in sorted_cities]), end="")
print("}", sep="", end="")

# This is substantially slower than the non-inlined version ... what's happening?
# It's Python 3.12 - there is no JIT ...

# 2004776 function calls in 2.236 seconds
#
#    Ordered by: standard name
#
#    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#         1    0.000    0.000    0.000    0.000 <frozen codecs>:260(__init__)
#         1    0.000    0.000    0.000    0.000 <frozen codecs>:309(__init__)
#      1589    0.001    0.000    0.004    0.000 <frozen codecs>:319(decode)
#      1589    0.000    0.000    0.000    0.000 <frozen codecs>:331(getstate)
#         1    0.000    0.000    2.236    2.236 <string>:1(<module>)
#         1    1.784    1.784    2.236    2.236 iteration2.py:8(doit)
#      1589    0.002    0.000    0.002    0.000 {built-in method _codecs.utf_8_decode}
#         1    0.000    0.000    0.000    0.000 {built-in method _io.open}
#         1    0.000    0.000    2.236    2.236 {built-in method builtins.exec}
#   1000000    0.120    0.000    0.120    0.000 {built-in method builtins.len}
#         1    0.000    0.000    0.000    0.000 {method '__exit__' of '_io._IOBase' objects}
#         1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
#   1000001    0.328    0.000    0.332    0.000 {method 'readline' of '_io.TextIOWrapper' objects}

# When profiled, iteration 1 is suddenly slower again ... with 1kk more function calls.
