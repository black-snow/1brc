# Just read it line by line, store stuff in a dict.
# Regex for parsing because it's less verbose.

import sys
import re

stats = {} # city => tuple of min, max, sum, count
pattern = re.compile(r"^([^;]+);(.*)\s?$")

with open(sys.argv[1], "r") as f:
    while (reading := f.readline()) != "":
        match = pattern.match(string=reading)
        city = match.group(1)
        temp = float(match.group(2))
        try:
            stat = stats[city]
        except KeyError:
            stat = (50.0, -20.0, .0, 0)
        stats[city] = (
            temp if temp < stat[0] else stat[0],
            temp if temp > stat[1] else stat[1],
            stat[2] + temp,
            stat[3] + 1
        )

sorted_cities = sorted(stats.keys())
print("{", sep="", end="")
print(", ". join([f"{city}={stats[city][0]}/{round(stats[city][2] / stats[city][3], 1)}/{stats[city][1]}" for city in sorted_cities]), end="")
print("}", sep="", end="")

# On my craptop this took 30 minutes! The heck? Would've expected 5-10. It's a T450s; bad but not too bad.
# Measuring shows that ~54% time of a loop (readline) is spent in the matching, ~29% in doing the stats.
# For 1kk rows we have 2.44 sec total, 1.3 for regex and 0.73 for stats. So 1kkk should take about
# 40 minutes - so 30 minutes without the code for measuring seems legit. Dang.

#
# Afterthoughts:
# - single threaded (or process ...), splitting and reading chunks concurrently will help
# - manual parsing should be faster than re
# - we silently assume no city has a semicolon in its name :D
# - floats could be off
# - can we do better than hashmap lookups w\o cheating?
# - actually, for most cases we don't need to check for BOTH min and max; if it is a new min it cannot be
#   a new max also (except for the first one) and vice versa
#
