<?php

$stats = [];

$fp = fopen($argv[1], "r");
while (($line = fgets($fp)) !== false) {
    $cityAndTemp = explode(";", $line, 2);
    $temp = floatval($cityAndTemp[1]);
    $stat = $stats[$cityAndTemp[0]] ?? [.0, .0, .0, 0];
    $stats[$cityAndTemp[0]] = [
        $temp < $stat[0] ? $temp : $stat[0],
        $temp > $stat[1] ? $temp : $stat[1],
        $stat[2] + $temp,
        ++$stat[3]
    ];
}
fclose($fp);

ksort($stats, SORT_STRING);

echo "{";
$i = 0;
$numCities = sizeof($stats);
foreach($stats as $city => $stat) {
    echo $city, "=", $stat[0], "/", number_format(round($stat[2] / $stat[3], 1), 1), "/", $stat[1], (++$i < $numCities ? ", " : "");
}
echo "}";

// inlined custom parsing: ~540ms - ~9 minutes - actually took 8:16
