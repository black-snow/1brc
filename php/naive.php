<?php

$stats = [];

$fp = fopen($argv[1], "r");
while (($line = fgets($fp)) !== false) {
    preg_match('/^([^;]+);(.*)\s?$/', $line, $matches);
    $stat = $stats[$matches[1]] ?? [.0, .0, .0, 0];
    $stats[$matches[1]] = [
        $matches[2] < $stat[0] ? $matches[2] : $stat[0],
        $matches[2] > $stat[1] ? $matches[2] : $stat[1],
        $stat[2] + $matches[2],
        ++$stat[3]
    ];
}
fclose($fp);

ksort($stats, SORT_STRING);

echo "{";
$i = 0;
$numCities = sizeof($stats);
foreach($stats as $city => $stat) {
    echo $city, "=", $stat[0], "/", number_format(round($stat[2] / $stat[3], 1), 1), "/", $stat[1], (++$i < $numCities ? ", " : "");
}
echo "}";

// takes ~930 ms on my craptop for 1kk lines, so 15ish minutes for 1kkk lines
