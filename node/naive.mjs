import fs from 'node:fs';
import readline from 'node:readline';
import process from 'node:process';

const stats = {};
const pattern = /^([^;]+);(.*)\s?$/;

async function readFile(stats) {
    const fileStream = fs.createReadStream(process.argv[2]); // why 2 node, why?
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: 0,
      escapeCodeTimeout: 0
    });
  
    for await (const line of rl) {
      let [_, city, temp] = line.match(pattern)
      temp = parseFloat(temp);
      const stat = stats[city] ?? [.0, .0, .0, 0]
      stats[city] = [
        temp < stat[0] ? temp : stat[0],
        temp > stat[1] ? temp : stat[1],
        temp + stat[2],
        ++stat[3]
      ]
    }
}
  
await readFile(stats);
const sortedCities = Object.keys(stats).sort()
const lastCity = sortedCities.pop()
process.stdout.write("{")
for (const i in sortedCities) {
  process.stdout.write(
    sortedCities[i] + "=" +
    stats[sortedCities[i]][0].toFixed(1) + "/" +
    (stats[sortedCities[i]][2] / stats[sortedCities[i]][3]).toFixed(1) + "/" +
    stats[sortedCities[i]][1].toFixed(1) + ", "
  )
}
process.stdout.write(
  lastCity + "=" +
  stats[lastCity][0].toFixed(1) + "/" +
  (stats[lastCity][2] / stats[lastCity][3]).toFixed(1) + "/" +
  stats[lastCity][1].toFixed(1)
)
process.stdout.write("}")

// about 1.3 seconds on my crapbook for 1kk - so 22-ish minutes for 1kkk
