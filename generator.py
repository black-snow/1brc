# The text file contains temperature values for a range of weather stations. 
# Each row is one measurement in the format <string: station name>;<double: measurement>,
# with the measurement value having exactly one fractional digit.
#  The following shows ten rows as an example:
#
# Hamburg;12.0
# Bulawayo;8.9
# Palembang;38.8
# St. John's;15.2
# Cracow;12.6
# Bridgetown;26.9
# Istanbul;6.2
# Roseau;34.4
# Conakry;31.2
# Istanbul;23.0
#
# Let's pick 30 stations and [-20, 50] degrees celsius.
# 
# Note that we sample such that every stations should just slightly vary
# around min=-20, max=+50, and avg=-15 - but who cares. We could make it
# more sophisticated to better see if we're making any mistakes in the
# implementations. Well ... maybe later.
#

import random

stations = [
    "Berlin",
    "London",
    "Chicago",
    "Paris",
    "Moskow",
    "Madrid",
    "Tokyo",
    "New York",
    "Sao Paulo",
    "Rome",
    "Budapest",
    "Shanghai",
    "Honolulu",
    "Krakow",
    "Stockholm",
    "Gothenburg",
    "Copenhagen",
    "Barcelona",
    "Cairo",
    "Cape Town",
    "Minsk",
    "Athen",
    "Riga",
    "Oslo",
    "Prague",
    "Mexico City",
    "Bagdad",
    "Kuala Lumpur",
    "Taipei",
    "Kampala"
]
temp_range = (-20., 50.)
num_samples = 1_000_000_000

print(f"num cities: {len(stations)}")
print(f"temp range: {temp_range}")
print(f"num samples: {num_samples}")

with open("measurements.csv", "x") as f:
    for i in range(num_samples):
        # will actually be skewed but we don't care
        temp = str(round(random.uniform(min(temp_range), max(temp_range)), 1))
        station = random.choice(stations)
        f.write(f"{station};{temp}\n")
