package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
)

type statistics struct {
	min float32
	max float32
	sum float32
	num int32
}

var stats map[string]statistics = make(map[string]statistics)

// cheap escape - we don't care about errors
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	regex := regexp.MustCompile(`^([^;]+);(.*)\s?$`)

	f, err := os.Open(os.Args[1])
	check(err)

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		matches := regex.FindStringSubmatch(line)
		city := matches[1]
		var temp float32
		temp64, _ := strconv.ParseFloat(matches[2], 32)
		temp = float32(temp64)

		var stat, exists = stats[city]
		if !exists {
			stat = statistics{
				min: temp,
				max: temp,
				sum: temp,
				num: 1,
			}
		} else {
			stat.sum += temp
			stat.num += 1
			if temp < stat.min {
				stat.min = temp
			}
			if temp > stat.max {
				stat.max = temp
			}
		}
		stats[city] = stat
	}

	// sort by cities
	cities := make([]string, 0, len(stats))
	for city := range stats {
		cities = append(cities, city)
	}
	sort.Strings(cities)

	fmt.Print("{")
	numCities := len(stats)
	i := 0
	for _, city := range cities {
		i++
		stat := stats[city]
		fmt.Printf("%s=%.2f/%.2f/%.2f", city, stat.min, (stat.sum / float32(stat.num)), stat.max)
		if i < numCities {
			fmt.Print(",")
		}

	}
	fmt.Print("}")
}

// runs in 1.3s from source and 850ms compiled on my craptop - so 14ish minutes for 1kkk
// that's just less that 10% faster than the naive PHP one

// the full thing actuall took 17m28s but didn't roast my craptop as much as the others ...
